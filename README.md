# Liste Themes Animegruppe

Sammlung der Links für den Adventskalender


# Firefox
## Actual Anime:
### [Your Name](https://addons.mozilla.org/de/firefox/addon/animated-kimi-no-na-wa/) (animiert)
### [Anime Girl Shooting Stars](https://addons.mozilla.org/de/firefox/addon/anime-girl-shooting-stars/)
### [Attack on Titan](https://addons.mozilla.org/de/firefox/addon/attack-on-titan/)
### [Sinon GGO](https://addons.mozilla.org/de/firefox/addon/g-g-o-sinon/)
### [S.A.O Asuna & Zekken (Yuuki)](https://addons.mozilla.org/de/firefox/addon/s-a-o-asuna-zekken/)
### [Attack on Titan](https://addons.mozilla.org/de/firefox/addon/attack-on-titan-animated/) (animiert)
### [Fairy Tail Naruto Sword Art Online](https://addons.mozilla.org/de/firefox/addon/anime-fairy-tail-naruto-swo/)

## Anime Themed:
### [Firefox-Nebula Blue](https://addons.mozilla.org/de/firefox/addon/anime-firefox-girl-nebula-b/)
### [Firefox-Nebula Red](https://addons.mozilla.org/de/firefox/addon/anime-firefox-girl-nebula-r/)

### [Diverse](https://addons.mozilla.org/de/firefox/user/12472712/)


